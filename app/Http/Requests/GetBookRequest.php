<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $title
 * @property mixed $authors
 * @property mixed $sortColumn
 * @property mixed $sortDirection
 */
class GetBookRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'string',
            'authors' => 'regex:/^\d(?:,\d)*$/',
            'sortColumn' => 'string|in:title,avg_review',
            'sortDirection' => 'string|in:asc,desc,ASC,DESC',
        ];
    }
}
