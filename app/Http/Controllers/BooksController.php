<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\GetBookRequest;
use App\Http\Requests\PostBookRequest;
use App\Http\Requests\PostBookReviewRequest;
use App\Http\Resources\BookResource;
use App\Http\Resources\BookReviewResource;
use Illuminate\Support\Facades\Auth;

class BooksController extends Controller
{
    public function getCollection(GetBookRequest $request)
    {
        $bookQuery = Book::with(['authors'])->withCount([
            'reviews as reviews_count',
            'reviews as avg_review' => function ($query) {
                return $query->select(\DB::raw('coalesce(avg(review),0)'));
            }
        ]);
        if ($request->has('title')) {
            $bookQuery->where('title', 'LIKE', '%' . $request->title . '%');
        }
        if ($request->has('authors')) {
            $authorIds = explode(',', $request->authors);
            $bookQuery->whereHas('authors', function ($query) use ($authorIds) {
                $query->whereIn('id', $authorIds);
            });
        }
        if ($request->has('sortColumn')) {
            $sortDirection = $request->has('sortDirection')
            && in_array(strtolower($request->sortDirection), ['asc', 'desc'])
                ? strtolower($request->sortDirection)
                : 'asc';
            $bookQuery->orderBy($request->sortColumn, $sortDirection);
        }
        return BookResource::collection($bookQuery->paginate());
    }

    public function post(PostBookRequest $request)
    {
        $book = Book::create([
            'isbn' => $request->isbn,
            'title' => $request->title,
            'description' => $request->description,
        ]);
        $book->authors()->attach($request->authors);
        return new BookResource($book);
    }

    public function postReview(Book $book, PostBookReviewRequest $request)
    {
        $review = $book->reviews()->create([
            'user_id' => Auth::guard('web')->id(),
            'comment' => $request->comment,
            'review' => $request->review
        ]);
        return new BookReviewResource($review);
    }
}
